require 'test_helper'

class GeneratesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @generate = generates(:one)
  end

  test "should get index" do
    get generates_url, as: :json
    assert_response :success
  end

  test "should create generate" do
    assert_difference('Generate.count') do
      post generates_url, params: { generate: { file: @generate.file } }, as: :json
    end

    assert_response 201
  end

  test "should show generate" do
    get generate_url(@generate), as: :json
    assert_response :success
  end

  test "should update generate" do
    patch generate_url(@generate), params: { generate: { file: @generate.file } }, as: :json
    assert_response 200
  end

  test "should destroy generate" do
    assert_difference('Generate.count', -1) do
      delete generate_url(@generate), as: :json
    end

    assert_response 204
  end
end
