# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_03_20_044824) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "brands", force: :cascade do |t|
    t.bigint "vtex_id", null: false
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_brands_on_name", unique: true
    t.index ["vtex_id"], name: "index_brands_on_vtex_id", unique: true
  end

  create_table "categories", force: :cascade do |t|
    t.bigint "vtex_id", null: false
    t.string "name", null: false
    t.bigint "parent_category_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name", "parent_category_id"], name: "index_categories_on_name_and_parent_category_id", unique: true
    t.index ["name"], name: "index_categories_on_name"
    t.index ["parent_category_id"], name: "index_categories_on_parent_category_id"
    t.index ["vtex_id"], name: "index_categories_on_vtex_id", unique: true
  end

  create_table "client_infos", force: :cascade do |t|
    t.string "first_name", null: false
    t.string "last_name", null: false
    t.string "document", null: false
    t.string "email", null: false
    t.string "address", null: false
    t.string "neighborhood", null: false
    t.string "city", null: false
    t.string "state", null: false
    t.string "country", null: false
    t.bigint "order_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "phone"
    t.index ["order_id"], name: "index_client_infos_on_order_id"
  end

  create_table "colors", force: :cascade do |t|
    t.string "id_erp"
    t.string "codigo_rgb"
    t.string "vtex_color"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "generates", force: :cascade do |t|
    t.string "file"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "guides", force: :cascade do |t|
    t.string "guide_number"
    t.string "response"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "guide_base64"
    t.string "manifest_base64"
    t.index ["guide_number"], name: "index_guides_on_guide_number"
  end

  create_table "images", force: :cascade do |t|
    t.string "url", null: false
    t.text "description"
    t.string "label"
    t.boolean "is_main", default: false
    t.bigint "sku_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "digest"
    t.index ["sku_id"], name: "index_images_on_sku_id"
  end

  create_table "order_items", force: :cascade do |t|
    t.bigint "vtex_id", null: false
    t.integer "quantity", null: false
    t.integer "price", null: false
    t.float "cubicweight", null: false
    t.integer "height", null: false
    t.integer "length", null: false
    t.integer "weight", null: false
    t.integer "width", null: false
    t.bigint "order_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "erp_id"
    t.integer "selling_price"
    t.index ["order_id"], name: "index_order_items_on_order_id"
  end

  create_table "orders", force: :cascade do |t|
    t.string "order_id", null: false
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "datetime"
    t.integer "value"
    t.integer "decimal_digits"
    t.string "creation_date"
    t.string "payment_method"
    t.integer "shipping_value"
    t.integer "items_value"
    t.integer "tax_value"
    t.integer "discounts_value"
    t.index ["order_id"], name: "index_orders_on_order_id", unique: true
  end

  create_table "products", force: :cascade do |t|
    t.bigint "vtex_id", null: false
    t.string "erp_id", null: false
    t.string "name", null: false
    t.string "text_link", null: false
    t.text "substite_words", null: false
    t.string "page_title", null: false
    t.text "description", null: false
    t.text "meta_description", null: false
    t.text "short_description", null: false
    t.date "release_date", null: false
    t.boolean "show_product", default: true, null: false
    t.boolean "show_unavailable", default: true, null: false
    t.integer "score"
    t.bigint "brand_id", null: false
    t.bigint "category_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "categoria_total"
    t.string "cares"
    t.index ["brand_id"], name: "index_products_on_brand_id"
    t.index ["category_id"], name: "index_products_on_category_id"
    t.index ["erp_id"], name: "index_products_on_erp_id", unique: true
    t.index ["name"], name: "index_products_on_name"
    t.index ["text_link"], name: "index_products_on_text_link", unique: true
    t.index ["vtex_id"], name: "index_products_on_vtex_id", unique: true
  end

  create_table "products_sync_logs", id: false, force: :cascade do |t|
    t.bigint "product_id", null: false
    t.bigint "sync_log_id", null: false
    t.index ["product_id"], name: "index_products_sync_logs_on_product_id"
  end

  create_table "skus", force: :cascade do |t|
    t.bigint "vtex_id", null: false
    t.string "erp_id", null: false
    t.string "name", null: false
    t.integer "price"
    t.integer "stock"
    t.decimal "weight", null: false
    t.decimal "height", null: false
    t.decimal "width", null: false
    t.decimal "length", null: false
    t.string "size"
    t.string "color"
    t.bigint "product_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_active"
    t.index ["erp_id"], name: "index_skus_on_erp_id", unique: true
    t.index ["product_id"], name: "index_skus_on_product_id"
    t.index ["vtex_id"], name: "index_skus_on_vtex_id", unique: true
  end

  create_table "sync_logs", force: :cascade do |t|
    t.bigint "timestamp", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["timestamp"], name: "index_sync_logs_on_timestamp"
  end

  add_foreign_key "categories", "categories", column: "parent_category_id"
  add_foreign_key "client_infos", "orders"
  add_foreign_key "images", "skus"
  add_foreign_key "order_items", "orders"
  add_foreign_key "products", "brands"
  add_foreign_key "products", "categories"
  add_foreign_key "skus", "products"
end
