class CreateClientInfos < ActiveRecord::Migration[5.2]
  def change
    create_table :client_infos do |t|
      t.string :first_name, null: false
      t.string :last_name, null: false
      t.string :document, null: false
      t.string :email, null: false
      t.string :address, null: false
      t.string :neighborhood, null: false
      t.string :city, null: false
      t.string :state, null: false
      t.string :country, null: false
      t.belongs_to :order, foreign_key: true, null: false

      t.timestamps
    end
  end
end
