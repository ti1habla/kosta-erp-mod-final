class AddDecimalDigitsToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :decimal_digits, :integer
  end
end
