class AddPdfsToGuide < ActiveRecord::Migration[5.2]
  def change
    add_column :guides, :guide_base64, :string
    add_column :guides, :manifest_base64, :string
  end
end
