class AddPhoneToClientInfos < ActiveRecord::Migration[5.2]
  def change
    add_column :client_infos, :phone, :string
  end
end
