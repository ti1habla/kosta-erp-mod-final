class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.string :order_id, null: false
      t.string :status, null: false

      t.timestamps
    end
    add_index :orders, :order_id, unique: true
  end
end
