class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.bigint :vtex_id, null: false
      t.string :erp_id, null: false
      t.string :name, null: false
      t.string :text_link, null: false
      t.text :substite_words, null: false
      t.string :page_title, null: false
      t.text :description, null: false
      t.text :meta_description, null: false
      t.text :short_description, null: false
      t.date :release_date, null: false
      t.boolean :show_product, null: false, default: true
      t.boolean :show_unavailable, null: false, default: true
      t.integer :score, null: true
      t.text :cares, array: true, default: []
      t.references :brand, foreign_key: true, null: false
      t.references :category, foreign_key: true, null: false

      t.timestamps
    end
    add_index :products, :vtex_id, unique: true
    add_index :products, :erp_id, unique: true
    add_index :products, :name, unique: true
    add_index :products, :text_link, unique: true
  end
end
