class CreateCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :categories do |t|
      t.bigint :vtex_id, null:false
      t.string :name, null: false
      t.references :parent_category, foreign_key: {to_table: :categories}, null: true

      t.timestamps
    end
    add_index :categories, :vtex_id, unique: true
    add_index :categories, :name
    add_index :categories, [:name, :parent_category_id], unique: true
  end
end
