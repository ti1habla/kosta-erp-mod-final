class CreateGuides < ActiveRecord::Migration[5.2]
  def change
    create_table :guides do |t|
      t.string :guide_number
      t.string :response

      t.timestamps
    end
    add_index :guides, :guide_number
  end
end
