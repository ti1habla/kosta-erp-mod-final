class CreateImages < ActiveRecord::Migration[5.2]
  def change
    create_table :images do |t|
      t.bigint :vtex_id, null: false
      t.string :url, null: false
      t.text :description, null: true
      t.string :label, null: true
      t.boolean :is_main, default: false
      t.references :sku, foreign_key: true, null: false

      t.timestamps
    end
  end
end
