class CreateProductsSyncLogsJoinTable < ActiveRecord::Migration[5.2]
  def change
    create_join_table :products, :sync_logs do |t|
      t.index :product_id
    end
  end
end
