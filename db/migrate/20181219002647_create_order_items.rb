class CreateOrderItems < ActiveRecord::Migration[5.2]
  def change
    create_table :order_items do |t|
      t.bigint :vtex_id, null: false
      t.integer :quantity, null: false
      t.integer :price, null: false
      t.float :cubicweight, null: false
      t.integer :height, null: false
      t.integer :length, null: false
      t.integer :weight, null: false
      t.integer :width, null: false
      t.belongs_to :order, foreign_key: true, null: false

      t.timestamps
    end
  end
end
