class AddColumnsToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :creation_date, :string
    add_column :orders, :payment_method, :string
    add_column :orders, :shipping_value, :integer
    add_column :orders, :items_value, :integer
    add_column :orders, :tax_value, :integer
    add_column :orders, :discounts_value, :integer
  end
end
