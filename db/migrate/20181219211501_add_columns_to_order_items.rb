class AddColumnsToOrderItems < ActiveRecord::Migration[5.2]
  def change
    add_column :order_items, :erp_id, :string
    add_column :order_items, :selling_price, :integer
  end
end
