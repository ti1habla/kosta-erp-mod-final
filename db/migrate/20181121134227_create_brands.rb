class CreateBrands < ActiveRecord::Migration[5.2]
  def change
    create_table :brands do |t|
      t.bigint :vtex_id, null: false
      t.string :name, null: false

      t.timestamps
    end
    add_index :brands, :vtex_id, unique: true
    add_index :brands, :name, unique: true
  end
end
