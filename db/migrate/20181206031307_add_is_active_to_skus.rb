class AddIsActiveToSkus < ActiveRecord::Migration[5.2]
  def change
    add_column :skus, :is_active, :boolean
  end
end
