class AddDigestToImages < ActiveRecord::Migration[5.2]
  def change
    add_column :images, :digest, :text
  end
end
