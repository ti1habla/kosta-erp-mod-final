class ChangeStatusNullOnOrders < ActiveRecord::Migration[5.2]
  def change
    change_column_null :orders, :status, true
  end
end
