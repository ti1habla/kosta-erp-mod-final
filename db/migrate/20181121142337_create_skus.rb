class CreateSkus < ActiveRecord::Migration[5.2]
  def change
    create_table :skus do |t|
      t.bigint :vtex_id, null: false
      t.string :erp_id, null: false
      t.string :name, null: false
      t.integer :price, null: true
      t.integer :stock, null: true
      t.decimal :weight, null: false
      t.decimal :height, null: false
      t.decimal :width, null: false
      t.decimal :length, null: false
      t.string :size, null: true
      t.string :color, null: true
      t.references :product, foreign_key: true, null: false

      t.timestamps
    end
    add_index :skus, :vtex_id, unique: true
    add_index :skus, :erp_id, unique: true
  end
end
