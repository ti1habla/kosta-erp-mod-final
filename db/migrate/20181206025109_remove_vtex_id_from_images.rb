class RemoveVtexIdFromImages < ActiveRecord::Migration[5.2]
  def change
    remove_column :images, :vtex_id, :bigint
  end
end
