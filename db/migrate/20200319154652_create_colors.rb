class CreateColors < ActiveRecord::Migration[5.2]
  def change
    create_table :colors do |t|
      t.string :id_erp
      t.string :codigo_rgb
      t.string :vtex_color

      t.timestamps
    end
  end
end
