Rails.application.routes.draw do
  resources :generates
  post 'orders/:order_id/invoice', to: 'orders#create_invoice'
  get '/sync_products', to: 'orders#sync_products'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api do
    namespace :v1 do
      scope 'tracking' do
        get '/', to: 'tracking#generateGuide'
      end
    end
  end

end
