require "net/http"

class ApiClient
  def self.get path, headers = {}
    request 'GET', path, headers
  end

  def self.put path, body, headers = {}
    request 'PUT', path, headers, body
  end

  def self.post path, body, headers = {}
    request 'POST', path, headers, body
  end

  def self.patch path, body, headers = {}
    request 'PATCH', path, headers, body
  end

  private

  def self.with_body_request uri, method, body
    request = nil
    case method
    when 'POST'
      request = Net::HTTP::Post.new uri.request_uri
    when 'PUT'
      request = Net::HTTP::Put.new uri.request_uri
    when 'PATCH'
      request = Net::HTTP::Patch.new uri.request_uri
    end

    if request != nil
      request['Content-Type'] = 'application/json'
      request.body = body.to_json
    end

    request
  end

  def self.request method, path, headers = {}, body = nil
    request = nil
    base_url = get_base_url

    # debug aquí

    uri = URI "#{base_url}#{path}"

    case method
    when 'GET'
      request = Net::HTTP::Get.new uri.request_uri
    when 'POST', 'PUT', 'PATCH'
      request = with_body_request uri, method, body
    end


    if request != nil
      default_headers = get_default_headers
      default_headers.each do |header_key, header_value|
        request[header_key] = header_value
      end

      headers.each do |header_key, header_value|
        request[header_key] = header_value
      end

      response = Net::HTTP.start uri.host, uri.port, use_ssl: uri.scheme == 'https' do |http|
        response = http.request request
      end

      content_type = response.content_type

      if content_type && content_type.index('application/json') != nil
        if response.class.body_permitted?
          body = response.body
          if body
            response.body = JSON.parse body, symbolize_names: true
          end
        end
      end

      response

      # abort response.body.inspect
    end
  end

  def self.get_base_url
    ""
    #"https://#{VTEX_STORE_NAME}.myvtex.com/api"
  end

  def self.get_default_headers
    
    {
      'X-VTEX-API-AppKey' => VTEX_APP_KEY,
      'X-VTEX-API-AppToken' => VTEX_APP_TOKEN,
    }
  end
end

