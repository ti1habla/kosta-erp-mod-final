module Erp
  module Api
    class MockResponse
      def initialize mock_data
        @mock_data = mock_data
      end

      def body
        @mock_data
      end
    end


    class Client < ApiClient
      API_TOKEN = KA_ERP_API_TOKEN


      def self.get_products_since timestamp = 0
        path = "#{ENV['KA_ERP_URL']}/referencias/api/productos?last_sync_date=#{ timestamp }"
        response = HTTParty.get(path, headers:{'Authorization' => "Bearer #{ENV['KA_ERP_API_TOKEN']}"})
    
        # MockResponse.new [
        #   {
        #     id: "123abc",
        #     nombre: "Producto de Prueba 1 editado",
        #     palabras_sustitutas: "conjunto,palabras,separadas,coma,cuales,puede,buscar,producto,aparte,nombre",
        #     text_link: "producto-prueba-1",
        #     titulo_de_la_pagina: "Producto de Prueba 1",
        #     descripcion_del_producto: "Este es un producto de prueba para la integración entre el ERP de Kosta Azul y el e-commerce de la misma marca en VTEX",
        #     meta_descripcion: "Este es un producto de prueba para la integración entre el ERP de Kosta Azul y el e-commerce de la misma marca en VTEX, entra y cómpralo ahora",
        #     marca: "Kosta Azul",
        #     categoria: "Formal/Camisas",
        #     fecha_de_lanzamiento: "12/01/2018",
        #     mostrar_producto: 1,
        #     mostrar_producto_agotado: 1,
        #     breve_descripcion: "descripcion corta que aparece en las vistas de catálogo de productos",
        #     score: 23,
        #     cuidados: [
        #       "Lavar solo a mano",
        #       "Planchar a 110 grados celsius maximo"
        #     ]
        #   },
        # ]
      end

      def self.get_skus_of product_id
        path = "#{ENV['KA_ERP_URL']}/referencias/api/productos/skus?product_id=#{product_id}"
        response = HTTParty.get(path, headers:{'Authorization' => "Bearer #{ENV['KA_ERP_API_TOKEN']}"})
        
        # MockResponse.new [
        #   {
        #     sku: "123abc-1",
        #     stock: 33,
        #     nombre: "Sku 1 editado",
        #     precio: 200000,
        #     peso: 500,
        #     alto: 20,
        #     ancho: 25,
        #     largo: 2,
        #     imagenes: [
        #       {
        #         url: "https://s3.amazonaws.com/cdatehortuab-public/images/123abc-1_a.jpg",
        #         texto: "Producto de prueba 1 Sku 1 Vista principal",
        #       },
        #       {
        #         url: "https://s3.amazonaws.com/cdatehortuab-public/images/123abc-1_b.jpg",
        #         texto: "Producto de prueba 1 Sku 1 Vista ampliada",
        #       },
        #       {
        #         url: "https://s3.amazonaws.com/cdatehortuab-public/images/123abc-1_c.jpg",
        #         texto: "Producto de prueba 1 Sku 1 Vista frontal",
        #       },
        #       {
        #         url: "https://s3.amazonaws.com/cdatehortuab-public/images/123abc-1_d.jpg",
        #         texto: "Producto de prueba 1 Sku 1 Vista trasera",
        #       },
        #       {
        #         url: "https://s3.amazonaws.com/cdatehortuab-public/images/123abc-1_d.jpg",
        #         texto: "Producto de prueba 1 Sku 1 Vista trasera",
        #       },
        #     ],
        #     talla: "XL",
        #     color: "Azul"
        #   },
        # ]
      end


      def self.create_order order_info
        path = "http://3.211.118.213:8080/comercial/vtex/order/"
        headers = {'Content-Type' => 'application/json', 'Authorization' => "Bearer #{ENV['KA_ERP_API_TOKEN']}"}

        # obj = {
        #   "orden_id": order_info[:orden_id]+"8",
        #   "fecha_creacion": order_info[:fecha_creacion],
        #   "cliente": {
        #     "nombre": order_info[:cliente][:nombre],
        #     "apellido": order_info[:cliente][:apellido],
        #     "cedula": order_info[:cliente][:cedula],
        #     "correo": order_info[:cliente][:correo],
        #     "direccion": order_info[:cliente][:direccion],
        #     "barrio": order_info[:cliente][:barrio],
        #     "edificio": "",
        #     "ciudad": order_info[:cliente][:ciudad],
        #     "departamento": order_info[:cliente][:departamento],
        #     "pais": order_info[:cliente][:pais]
        #   },
        #   "items": [
        #     {
        #       "cantidad": order_info[:items][0][:cantidad],
        #       "sku": order_info[:items][0][:sku],
        #       "precio_de_venta": order_info[:items][0][:precio_de_venta],
        #       "precio": order_info[:items][0][:precio]
        #     } # Falta validar cuando son varios productos
        #   ],
        #   "valor_total": order_info[:valor_total],
        #   "metodo_de_pago": order_info[:metodo_de_pago]
        # }
        
        #abort order_info.inspect

        
        response = HTTParty.post(path, :body => order_info.to_json, :headers => headers)
      end

      def self.update_order_status order_id, status
        path = "http://3.211.118.213:8080/comercial/vtex/order/"
        headers = {'Authorization' => "Bearer #{ENV['KA_ERP_API_TOKEN']}"}
        response = HTTParty.put(path, :body => order_info, :headers => headers)
        # body = {
        #   orden_id: order_id,
        #   estado: status,
        # }
        # patch path, body
        # puts path, body
        # MockResponse.new({
        #   resultado: 'ok'
        # })
      end

      private

      def self.get_base_url
        ENV['KA_ERP_URL']
      end

      def self.get_default_headers
        {
          'Authorization' => "Bearer #{ENV['KA_ERP_API_TOKEN']}"
        }
      end
    end
  end
end
