module Vtex
  module WebService
    module Serialization
      def self.serialize_especification id_value, field_name, field_value, isSku = false
        id_key = isSku ? "tem:idSku" : "tem:idProduct"
        {
          "#{id_key}" => id_value,
          "tem:fieldName" => field_name,
          "tem:fieldValues" => {
            "arr:string" => field_value
          }
        }
      end
    end
  end
end

