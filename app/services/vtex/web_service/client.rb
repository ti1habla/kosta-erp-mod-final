require 'savon'

module Vtex
  module WebService
    class Client
      SAVON_CONFIG = {
        wsdl: #"https://webservice-kostaazul.vtexcommerce.com.br/AdminWebService/Service.svc?wsdl",
              "https://webservice-kostaazul.vtexcommerce.com.br/service.svc?wsdl",
        basic_auth: [VTEX_APP_KEY, VTEX_APP_TOKEN],
        namespace: "http://tempuri.org/",
        namespace_identifier: :tem,
        namespaces: {
          "xmlns:vtex" => "http://schemas.datacontract.org/2004/07/Vtex.Commerce.WebApps.AdminWcfService.Contracts",
          "xmlns:arr" => "https://kostazul.s3.amazonaws.com/sitioweb/serialization_arrays"
        },
        env_namespace: :soapenv,
      }.freeze

      @@savon_client = Savon.client SAVON_CONFIG


      def self.insert_or_update_product product_hash
        @@savon_client.call(:product_insert_update, message: product_hash)
      end

      def self.insert_or_update_product_and_get_id product_hash
        response = insert_or_update_product product_hash
        response_body = response.body
        response_body[:product_insert_update_response][:product_insert_update_result][:id]
      end

      def self.insert_product_especification product_especification_hash
        @@savon_client.call(:product_especification_insert, message: product_especification_hash)
      end

      def self.insert_or_update_sku sku_hash
        @@savon_client.call(:stock_keeping_unit_insert_update, message: sku_hash)
      end

      def self.insert_or_update_sku_and_get_result sku_hash
        response = insert_or_update_sku sku_hash
        response_body = response.body
        response_body[:stock_keeping_unit_insert_update_response][:stock_keeping_unit_insert_update_result]
      end

      def self.activate_sku sku_id
        @@savon_client.call(:stock_keeping_unit_active, message: { 'tem:idStockKeepingUnit' => sku_id })
      end

      def self.insert_sku_especification sku_especification_hash

        pp sku_especification_hash.inspect

        # new_serialization = {
        #   'tem:idSku' => sku_especification_hash["tem:idSku"],
        #   'tem:fieldName' => sku_especification_hash["tem:idSku"],
        #   'tem:fieldValues' => "30" 
        # }

        # new_serialization = "<!--number, SKU identifier-->
        # <tem:idSku>#{sku_especification_hash["tem:idSku"]}</tem:idSku>
        # <!--string, field identifier, field name-->
        # <tem:fieldName>74</tem:fieldName>
        # <tem:fieldValues>30</tem:fieldValues>"

        #pp new_serialization
        @@savon_client.call(:stock_keeping_unit_especification_insert, message: sku_especification_hash)
        #pp @@savon_client.call(:stock_keeping_unit_especification_insert, message: new_serialization)

        abort
      end

      # schemas microsoft serialization arrays not found

      def self.insert_or_update_image image_hash
        begin
          @@savon_client.call(:image_insert_update, message: image_hash)
        rescue
          pp "error al procesar la entidad: image_insert_update"
        end
      end
    end
  end
end
