module Vtex
  module Api
    class Client < ApiClient
      def self.get_category_tree category_levels = 3
        path = "https://#{VTEX_STORE_NAME}.myvtex.com/api/catalog_system/pub/category/tree/#{category_levels}"
        ApiClient::get path
      end

      def self.get_brands
        path = "https://#{VTEX_STORE_NAME}.myvtex.com/api/catalog_system/pvt/brand/list"
        ApiClient::get path
      end

      def self.update_sku_stock sku_id, stock, inventory_id = VTEX_DEFAUL_INVENTORY_ID
        require 'httparty'
        # path = "https://#{VTEX_STORE_NAME}.myvtex.com/api/logistics/pvt/inventory/skus/#{sku_id}/warehouses/#{inventory_id}"
        path = "https://logistics.myvtex.com/api/logistics/pvt/inventory/skus/#{sku_id}/warehouses/#{inventory_id}?an=kostaazul"
        headers = { 'X-VTEX-API-AppKey' => VTEX_APP_KEY, 'X-VTEX-API-AppToken' => VTEX_APP_TOKEN }
        body_json = {
            "unlimitedQuantity": false,
            "dateUtcOnBalanceSystem": nil,
            "quantity": stock
          }
        
        response = HTTParty.put(path, :body => body_json, :headers => headers)
      end

      def self.update_sku_price sku_id, price
        require 'httparty'
        # old_path = "https://#{VTEX_STORE_NAME}.myvtex.com/api/pricing/pvt/price-sheet/"
        path = "https://api.vtex.com/#{VTEX_STORE_NAME}/pricing/prices/#{sku_id}"
        headers = { 'X-VTEX-API-AppKey' => VTEX_APP_KEY, 'X-VTEX-API-AppToken' => VTEX_APP_TOKEN }
        body = { 'costPrice' => price, 'listPrice' => 0, 'markup' => 0 }
        response = HTTParty.put(path, :body => body.to_json, :headers => headers)
      end

      def self.get_orders_feed max_lot = 100000 
        #old_path = 'https://#{VTEX_STORE_NAME}.myvtex.com/api/oms/pvt/feed/orders/status'
        path = "https://kostaazul.vtexcommercestable.com.br/api/oms/pvt/orders?_stats=1&f_creationDate=creationDate:[2019-11-07T02:00:00.000Z TO 2030-11-08T01:59:59.999Z]&orderBy=creationDate,desc&page=1"
        headers = { 'X-VTEX-API-AppKey' => VTEX_APP_KEY, 'X-VTEX-API-AppToken' => VTEX_APP_TOKEN }
        response = HTTParty.get(path, headers: headers)
      end

      def self.get_order_detail order_id
        # old_path = "https://kostaazul.vtexcommercestable.com.br/api/oms/pvt/orders/#{order_id}"
        path = "https://kostaazul.vtexcommercestable.com.br/api/oms/pvt/orders/#{order_id}"

        headers = { 'X-VTEX-API-AppKey' => VTEX_APP_KEY, 'X-VTEX-API-AppToken' => VTEX_APP_TOKEN }
        response = HTTParty.get(path, headers: headers)
      end

      def self.order_start_handling order_id
        # old_path = "https://#{VTEX_STORE_NAME}.myvtex.com/api/oms/pvt/orders/#{order_id}/start-handling"
        path = "https://kostaazul.vtexcommercestable.com.br/api/oms/pvt/orders/#{order_id}/start-handling"
        
        headers = { 'X-VTEX-API-AppKey' => VTEX_APP_KEY, 'X-VTEX-API-AppToken' => VTEX_APP_TOKEN }
        body = { }
        response = HTTParty.post(path, :body => body.to_json, :headers => headers)
      end

      def self.ack_orders_feed commit_tokens
        # old_path = 'https://#{VTEX_STORE_NAME}.myvtex.com/api/oms/pvt/feed/orders/status/confirm'
        path = "https://kostaazul.vtexcommercestable.com.br/api/oms/pvt/feed/orders/status/confirm"

        body = commit_tokens.map do |commit_token|
          {
            commitToken: commit_token
          }
        end

        post path, body
      end

      def self.invoice_order order_id, invoice_data
        path = "https://#{VTEX_STORE_NAME}.myvtex.com/api/oms/pvt/orders/#{order_id}/invoice"

        post path, invoice_data
      end



      def self.insert_size sku_id, size
        path = "https://kostaazul.vtexcommercestable.com.br/api/catalog/pvt/stockkeepingunit/#{sku_id}/specification"
        headers = { 'X-VTEX-API-AppKey' => VTEX_APP_KEY, 'X-VTEX-API-AppToken' => VTEX_APP_TOKEN }

        path_fields = "https://kostaazul.vtexcommercestable.com.br/api/catalog_system/pub/specification/fieldvalue/78"
        getFieldValueId = HTTParty.get(path_fields, headers: headers)

        fieldvalueidlocal = 0
        getFieldValueId.each do |gfv|
          if gfv["Value"] == size.to_s
            fieldvalueidlocal = gfv["FieldValueId"]
          end
        end
        
        body_json = {
          "FieldId": 78,
          "FieldValueId": fieldvalueidlocal
        }
        response = HTTParty.post(path, :body => body_json, :headers => headers)
      end



      
      def self.insert_color sku_id, color
        path = "https://kostaazul.vtexcommercestable.com.br/api/catalog/pvt/stockkeepingunit/#{sku_id}/specification"
        headers = { 'X-VTEX-API-AppKey' => VTEX_APP_KEY, 'X-VTEX-API-AppToken' => VTEX_APP_TOKEN }

        path_fields = "https://kostaazul.vtexcommercestable.com.br/api/catalog_system/pub/specification/fieldvalue/77"
        getFieldValueId = HTTParty.get(path_fields, headers: headers)
        
        fieldvalueidlocal = 'Otro'
        getFieldValueId.each do |gfv|
          begin
            if gfv["Value"] == Color.find(color).vtex_color.titleize
              fieldvalueidlocal = gfv["FieldValueId"]
            end
          rescue
            fieldvalueidlocal = 'Otro'
          end
        end
        
        body_json = {
          "FieldId": 77,
          "FieldValueId": fieldvalueidlocal
        }
        response = HTTParty.post(path, :body => body_json, :headers => headers)
      end




      def self.insert_product_specification product_id, cares
        path = "https://kostaazul.vtexcommercestable.com.br/api/catalog/pvt/product/#{product_id}/specification"
        headers = { 'X-VTEX-API-AppKey' => VTEX_APP_KEY, 'X-VTEX-API-AppToken' => VTEX_APP_TOKEN }

        path_fields = "https://kostaazul.vtexcommercestable.com.br/api/catalog_system/pub/specification/fieldvalue/76"
        getFieldValueId = HTTParty.get(path_fields, headers: headers)
        
        fieldvalueidlocal = 0
        fieldvaluetextlocal = ""
        getFieldValueId.each do |gfv|
          if gfv["Position"].to_s == cares.to_s
            fieldvalueidlocal = gfv["FieldValueId"]
            fieldvaluetextlocal = gfv["Value"]
          end
        end
        
        body_json = {
          "Fieldid": 76,
          "Fieldvalueid": fieldvalueidlocal,
          "Text": fieldvaluetextlocal
        }
        response = HTTParty.post(path, :body => body_json, :headers => headers)
      end



      

      private

      def self.get_base_url
        "https://#{VTEX_STORE_NAME}.myvtex.com/api"
      end

      def self.get_default_headers
        {
          'X-VTEX-API-AppKey' => VTEX_APP_KEY,
          'X-VTEX-API-AppToken' => VTEX_APP_TOKEN,
        }
      end
    end
  end
end
