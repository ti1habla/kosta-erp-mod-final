class OrdersController < ApplicationController

  def sync_products
    # %x[rake ka_integration:sync_products_and_skus_from_erp_to_vtex]

    require 'rake'
    KostaAzulErpIntegration::Application.load_tasks

    #Rake::Task['ka_integration:sync_products_and_skus_from_erp_to_vtex'].invoke
    Rake::Task['ka_integration:sync_products_and_skus_from_erp_to_vtex'].execute

  end
  
  def create_invoice
    order_id = params[:order_id]
    auth_header = request.headers()['Authorization']

    if !KA_ERP_API_TOKEN || KA_ERP_API_TOKEN == ''
      render json: {
        codigo: 501,
        resultado: 'error',
        mensaje: 'There is not a method for Authentication',
      }, status: 501
    elsif "Bearer #{KA_ERP_API_TOKEN}" != auth_header
      render json: {
        codigo: 401,
        resultado: 'error',
        mensaje: 'Unauthorized',
      }, status: 401
    else
      order = Order.find_by order_id: order_id
      if !order
        render json: {
          codigo: 404,
          resultado: 'error',
          mensaje: 'Order not found',
        }, status: 404
      else
        issuance_date = params[:fecha_facturacion]
        invoice_number = params[:numero_factura]
        invoice_url = params[:url_factura]

        if !issuance_date || !invoice_number || issuance_date == '' || invoice_number == ''
          render json: {
            codigo: 4000,
            resultado: 'error',
            mensaje: 'fecha_facturacion y numero_factura son obligatorios',
          }, status: 400
        else

          invoice_data = {
            type: 'Output',
            issuanceDate: issuance_date,
            invoiceNumber: invoice_number,
            invoiceValue: order.value,
            invoiceUrl: invoice_url || nil,
            # TODO: Get tracking number and tracking url
            # courier: 'Servientrega',
            # trackingNumber:  nil,
            # trackingUrl:  nil,
          }

          begin
            response = Vtex::Api::Client.invoice_order order.order_id, invoice_data
            case response
            when Net::HTTPSuccess
              order.status = 'invoiced'
              order.datetime = DateTime.now
              order.save

              render json: {
                resultado: 'ok'
              }, status: 200
            else
              render json: {
                codigo: 5000,
                resultado: 'error',
                mensaje: 'VTEX respondió con un error, inténtelo más tarde'
              }, status: 500
            end
          rescue
            render json: {
              codigo: 5001,
              resultado: 'error',
              mensaje: 'Error desconocido al intentar contactar a VTEX, inténtelo más tarde'
            }, status: 500
          end
        end
      end
    end
  end
end
