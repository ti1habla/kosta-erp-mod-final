class GeneratesController < ApplicationController
  before_action :set_generate, only: [:show, :update, :destroy]

  # GET /generates
  def index
      @skus = Sku.all
    render json: @skus
  end

  # GET /generates/1
  def show
    render json: @generate
  end

  # POST /generates
  def create
    @generate = Generate.new(generate_params)

    if @generate.save
      render json: @generate, status: :created, location: @generate
    else
      render json: @generate.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /generates/1
  def update
    if @generate.update(generate_params)
      render json: @generate
    else
      render json: @generate.errors, status: :unprocessable_entity
    end
  end

  # DELETE /generates/1
  def destroy
    @generate.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_generate
      @generate = Generate.first
    end

    # Only allow a trusted parameter "white list" through.
    def generate_params
      params.require(:generate).permit(:file)
    end
end
