
require 'savon'
class Api::V1::TrackingController < ApplicationController

    SAVON_CONFIG = {
        wsdl: "http://web.servientrega.com:8081/GeneracionGuias.asmx?WSDL",
        
        soap_header: {
            "tem:AuthHeader" => {
                "tem:login" => "Luis1937", "tem:pwd" => "MZR0zNqnI/KplFlYXiFk7m8/G/Iqxb3O", "tem:Id_CodFacturacion" => "SER408", "tem:Nombre_Cargue" => "NUEVA_VERSION_JMGP"
            }
        },
        namespace: "http://tempuri.org/",
        namespace_identifier: :tem,
        env_namespace: "SOAP-ENV"
    }
    @@client = Savon.client SAVON_CONFIG

    def self.generateGuide(data)
        total_volume = data[:total_volume] 
        total_weight = data[:total_weight]
        total_height = data[:total_height]
        total_width = data[:total_width]
        total_length = data[:total_length]
        items_quantity = data[:items_quantity]
        total_price = data[:total_price]
        first_name = data[:first_name] 
        last_name = data[:last_name] 
        document = data[:document]
        email = data[:email]
        address = data[:address]
        neighborhood = data[:neighborhood]
        city = data[:city] 
        state = data[:state] 
        country = data[:country]

        data = {
            "tem:envios" => {
                "tem:CargueMasivoExternoDTO" => {
                    "tem:objEnvios" => {
                        "tem:EnviosExterno" => {
                            "tem:Num_Guia" => "0",
                            "tem:Num_Sobreporte" => "0",
                            "tem:Num_Piezas" => items_quantity,
                            "tem:Des_TipoTrayecto" => "1",
                            "tem:Ide_Producto" => "2",
                            "tem:Ide_Destinatarios" => "00000000-0000-0000-0000-000000000000",
                            "tem:Ide_Manifiesto" => "00000000-0000-0000-0000-000000000000",
                            "tem:Des_FormaPago" => "2",
                            "tem:Des_MedioTransporte" => "1",
                            "tem:Num_PesoTotal" => total_weight,
                            "tem:Num_ValorDeclaradoTotal" => total_price,
                            "tem:Num_VolumenTotal" => total_volume,
                            "tem:Num_BolsaSeguridad" => "0",
                            "tem:Num_Precinto" => "0",
                            "tem:Des_TipoDuracionTrayecto" => "1",
                            "tem:Des_Telefono" => "",
                            "tem:Des_Ciudad" => city,
                            "tem:Des_Direccion" => address,
                            "tem:Nom_Contacto" => "#{first_name} #{last_name}",
                            "tem:Num_ValorLiquidado" => "0",
                            "tem:Des_DiceContener" => "VESTUARIO",
                            "tem:Des_TipoGuia" => "0",
                            "tem:Num_VlrSobreflete" => "0",
                            "tem:Num_VlrFlete" => "8800",
                            "tem:Num_Descuento" => "0",
                            "tem:idePaisOrigen" => "1",
                            "tem:idePaisDestino" => "1",
                            "tem:Des_IdArchivoOrigen" => "1",
                            "tem:Des_DireccionRemitente" => "",
                            "tem:Num_PesoFacturado" => "0",
                            "tem:Est_CanalMayorista" => "false",
                            "tem:Num_Alto" => total_height,
                            "tem:Num_Ancho" => total_width,
                            "tem:Num_Largo" => total_length,
                            "tem:Des_DepartamentoDestino" => state,
                            "tem:Gen_Cajaporte" => "false",
                            "tem:Gen_Sobreporte" => "false",
                            "tem:Nom_UnidadEmpaque" => "GENERICO",
                            "tem:Des_UnidadLongitud" => "cm",
                            "tem:Des_UnidadPeso" => "kg",
                            "tem:Num_ValorDeclaradoSobreTotal" => "0",
                            "tem:Num_Factura" => "FACT-001",
                            "tem:Des_CorreoElectronico" => email,
                            "tem:Num_Recaudo" => "0",
                            "tem:objEnviosUnidadEmpaqueCargue" => {
                                "tem:EnviosUnidadEmpaqueCargue" => {
                                    "tem:Num_Alto" => total_height,
                                    "tem:Num_Distribuidor" => "0",
                                    "tem:Num_Ancho" => total_width,
                                    "tem:Num_Cantidad" => "1",
                                    "tem:Des_DiceContener" => "TOMA DE MUESTRA",
                                    "tem:Des_IdArchivoOrigen" => "1",
                                    "tem:Num_Largo" => total_length,
                                    "tem:Nom_UnidadEmpaque" => "GENERICA",
                                    "tem:Num_Peso" => total_weight,
                                    "tem:Des_UnidadLongitud" => "cm",
                                    "tem:Des_UnidadPeso" => "kg",
                                    "tem:Ide_UnidadEmpaque" => "00000000-0000-0000-0000-000000000000",
                                    "tem:Ide_Envio" => "00000000-0000-0000-0000-000000000000",
                                    "tem:Num_Volumen" => total_volume,
                                    "tem:Num_Consecutivo" => "0",
                                    "tem:Num_ValorDeclarado" => total_price
                                }
                            }
                        }
                    }
                }
            }
        }
        response = @@client.call(:cargue_masivo_externo, message: data)
        puts "***********************"
        guide_number = nil
        if response && response.body && response.body[:cargue_masivo_externo_response]
            guide_number = response.body[:cargue_masivo_externo_response][:envios][:cargue_masivo_externo_dto][:obj_envios][:envios_externo][:num_guia]
            Guide.create(guide_number: guide_number, response: response.body)
            puts guide_number
            file_print_guide = printGuide(guide_number)
            file_manifest = generateManifest(guide_number)
            Guide.where(guide_number: guide_number).first_or_initialize.tap do |guide|
                guide.guide_number = guide_number
                # guide.guide_base64 = file_print_guide[:base64]
                # guide.manifest_base64 = file_manifest[:base64]
                guide.save
            end
            return {
                file_print_guide: file_print_guide[:file],
                file_manifest: file_manifest[:file],
                guide_number: guide_number
            }
        end
        return nil
    end


    private
        def self.printGuide(guide)
            data = {
                "num_Guia" => guide,
                "num_GuiaFinal" => guide,
                "ide_CodFacturacion" => 'SER408',
                "sFormatoImpresionGuia" => 2,
                "interno" => false
            }
            response = @@client.call(:generar_guia_sticker, message: data)
            bytes_pdf = response.body[:generar_guia_sticker_response][:bytes_report]
            raw_pdf_str = Base64.decode64 bytes_pdf
            file = File.open("public/guides/print_guide_#{guide}.pdf", 'wb') do |f|
                f.write(Base64.decode64(bytes_pdf))
            end
            return {
                file: file,
                base64: raw_pdf_str
            }
        end

        def self.generateManifest(guide)
            data = {
                "tem:Ide_Currier" => "111195110",
                "tem:Nombre_Currier" => "JUAN DAVID SILVA",
                "tem:Ide_Auxiliar" => "0",
                "tem:Placa_Vehiculo" => "SMP854",
                "tem:Lista_Guias_Xml" => {
                    "tem:Guias" => {
                        "tem:ObjetoGuia" => {
                            "tem:Numero_Guia" => guide
                        }
                    }   
                }
            }
            response = @@client.call(:generar_manifiesto, message: data)
            bytes_pdf = response.body[:generar_manifiesto_response][:cadena_bytes]
            raw_pdf_str = Base64.decode64 bytes_pdf
            file = File.open("public/manifests/manifest_#{guide}.pdf", 'wb') do |f|
                f.write(Base64.decode64(bytes_pdf))
            end
            return {
                file: file,
                base64: raw_pdf_str
            }
        end
end