class OrdersMailer < ApplicationMailer

    def send_guide_servientrega(email, guide_number)
        @email = email
        @guide_number = guide_number
        mail to: [@email], subject: "Tu pedido va en camino!!", from: 'Kosta Azul <info@kostazul.com>'
    end


    def skus_no_saved(collect_skus)
        @collect_skus = collect_skus
        mail to: "tecnologia@kostazul.com", subject: "Skus no sincronizados", from: 'Kosta Azul <info@kostazul.com>'
    end

end
