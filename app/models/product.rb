class Product < ApplicationRecord
  belongs_to :brand
  belongs_to :category
  has_many :skus, inverse_of: :product

  include PersistedComparable

  MAP_KEYS_ERP = {
    erp_id: :id,
    name: :nombre,
    text_link: :text_link,
    substite_words: :palabras_sustitutas,
    page_title: :titulo_de_la_pagina,
    description: :descripcion_del_producto,
    meta_description: :meta_descripcion,
    short_description: :breve_descripcion,
    release_date: :fecha_de_lanzamiento,
    show_product: :mostrar_producto,
    show_unavailable: :mostrar_producto_agotado,
    score: :score,
    cares: :cuidados
  }.freeze

  def vtex_web_service_serialize
    # pp "***********"
    # pp category
    # pp erp_id
    # pp "***"
    {
      "tem:productVO" => {
        "vtex:BrandId" => brand.vtex_id,
        "vtex:CategoryId" => category.vtex_id,
        "vtex:DepartmentId" => category.get_top_category.vtex_id,
        "vtex:Description" => description.gsub("descripcion buscadores field ", ""),
        "vtex:DescriptionShort" => short_description.gsub("descripcion buscadores field ", ""),
        "vtex:Id" => vtex_id,
        "vtex:IsActive" => show_product,
        "vtex:IsVisible" => show_product,
        "vtex:KeyWords" => substite_words,
        "vtex:LinkId" => text_link,
        "vtex:MetaTagDescription" => meta_description,
        "vtex:Name" => name,
        "vtex:RefId" => erp_id,
        "vtex:ReleaseDate" => release_date,
        "vtex:Score" => score,
        "vtex:ShowWithoutStock" => show_unavailable,
        "vtex:Title" => page_title,
        "vtex:ListStoreId" => {
            "arr:int" => "1"
          }
      }
    }
  end

  def vtex_web_service_serialize_cares
    Vtex::WebService::Serialization.serialize_especification vtex_id, 'Cuidados', cares
  end

  def data_modified?
    modified?(:name) ||
    modified?(:text_link) ||
    modified?(:substite_words) ||
    modified?(:page_title) ||
    modified?(:description) ||
    modified?(:meta_description) ||
    modified?(:short_description) ||
    modified?(:release_date) ||
    modified?(:show_product) ||
    modified?(:show_unavailable) ||
    modified?(:score) ||
    modified?(:category) ||
    modified?(:brand) ||
    modified?(:vtex_id) ||
    modified?(:erp_id)
  end

  def cares_modified?
    modified? :cares
  end

  def self.new_from_erp_format erp_product
    categoria_manual = Category.get_from_erp_format erp_product["categoria"]
    # pp categoria_manual
    # if "79409" == erp_product["id"]
    #   abort
    # end


    #marca_manual = Brand.find_by name: erp_product[:marca]
    if erp_product["fecha_de_lanzamiento"]
      lanzamiento = erp_product["fecha_de_lanzamiento"]
    else
      lanzamiento = Time.now.strftime("%d"+"/"+"%m"+"/"+"%Y")
    end

    nombre_mod = erp_product["nombre"] == "Formal" ? "Formal: "+rand(1..3000).to_s : erp_product["nombre"]

    if !erp_product["fecha_de_lanzamiento"].nil?
      if erp_product["fecha_de_lanzamiento"] <= Time.now.strftime("%Y"+"-"+"%m"+"-"+"%d")
        # "menor"
        show_product = 1 #se activa de una
      else
        # "mayor"
        show_product = 0
      end
    else
      show_product = 1
    end
    
    @product_link = erp_product["titulo_de_la_pagina"].gsub(" ", "-").gsub(":","").downcase+"-"+rand(1..20).to_s
    # abort @product_link.inspect

    product_hash = {
      erp_id: erp_product["id"],
      name: nombre_mod,
      text_link: @product_link, #erp_product["text_link"],
      substite_words: erp_product["palabras_sustitutas"],
      page_title: erp_product["titulo_de_la_pagina"],
      description: erp_product["descripcion_del_producto"].gsub("descripcion buscadores field ", ""),
      meta_description: erp_product["meta_descripcion"],
      short_description: erp_product["breve_descripcion"],
      release_date: lanzamiento,
      show_product: show_product,
      show_unavailable: erp_product["mostrar_producto_agotado"],
      score: erp_product["score"],
      cares: erp_product["cuidado"],
      category: categoria_manual,
      brand: Brand.first,
      categoria_total: erp_product["categoria"]
    }

    # product_hash[:category] = Category.get_from_erp_format erp_product[:categoria]
    # product_hash[:brand] = Brand.find_by name: erp_product[:marca]

    new product_hash

  end

  def self.new_and_persisted_modified products
    super(products, :erp_id, [:vtex_id])
  end
end
