class Image < ApplicationRecord
  belongs_to :sku, inverse_of: :images

  include PersistedComparable

  MAP_KEYS_ERP = {
    url: :url,
    description: :texto,
    # label: :titulo,
    # is_main: :is_main,
  }.freeze

  def vtex_web_service_serialize
    # pp url
    {
      "tem:image" => {
        "vtex:IsMain" => is_main,
        "vtex:Label" => label,
        "vtex:Name" => description,
        "vtex:StockKeepingUnitId" => sku.vtex_id,
        "vtex:Url" => url,
      }
    }
  end

  def data_modified?
    modified?(:url) ||
    modified?(:description) ||
    modified?(:label) ||
    modified?(:is_main)
  end

  def add_label_and_is_main_by_filename
    filename = self.url.split('/').last
    image_consecutive = /^.*_([a-z]+)\.(?:jpg|png|PNG|JPG|jpeg|JPEG)$/.match(filename)[1]

    case image_consecutive
    when 'a'
      self.label = 'Main'
      self.is_main = true
    when 'b'
      self.label = 'Hover'
      self.is_main = false
    else
      self.label = nil
      self.is_main = false
    end
  end

  def compute_and_add_digest
    string = "#{self.url}/#{self.description}/#{self.label}/#{self.is_main}"
    self.digest = Digest::SHA256.hexdigest string
  end

  def self.new_from_erp_format erp_image
    # image_hash = {}
    # MAP_KEYS_ERP.each do |key_model, key_erp|
    #   image_hash[key_model] = erp_image[key_erp]
    # end

    image_hash = {
      # url: ENV["KA_ERP_URL"] + erp_image["url"],
      url: erp_image["url"],
      description: erp_image["texto"].gsub(".", ""),
      label: erp_image["titulo"]
    }

    image = new image_hash

    image.add_label_and_is_main_by_filename
    image.compute_and_add_digest

    image
  end
end
