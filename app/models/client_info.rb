class ClientInfo < ApplicationRecord
  belongs_to :order, inverse_of: :client_info

  def erp_serialize
    {
      nombre: self.first_name,
      apellido: self.last_name,
      cedula: self.document,
      correo: self.email,
      direccion: self.address,
      barrio: self.neighborhood,
      edificio: "",
      ciudad: self.city,
      departamento: self.state,
      pais: self.country,
    }
  end
end
