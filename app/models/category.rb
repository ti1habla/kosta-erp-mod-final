class Category < ApplicationRecord
  NIL_CATEGORY = Category.new.freeze

  belongs_to :parent_category, class_name: 'Category', inverse_of: :children_categories
  has_many(
    :children_categories,
    class_name: 'Category',
    foreign_key: :parent_category_id,
    inverse_of: :parent_category
  )

  def initialize(attrs, *rest)
    attrs = attrs || {}
    attrs[:parent_category] = attrs[:parent_category] || NIL_CATEGORY

    super attrs, *rest
  end

  def has_parent_category?
    parent_category != nil && parent_category != NIL_CATEGORY
  end

  def parent_category=(new_parent_category, *rest)
    if new_parent_category == nil
      new_parent_category = NIL_CATEGORY
    end
    super new_parent_category, *rest
  end

  def get_top_category
    parent = self
    while parent.has_parent_category?
      parent = parent.parent_category
    end

    parent
  end

  def self.get_from_erp_format category_erp_format

    if category_erp_format == nil || category_erp_format == ""
      Category.second
    else
      #category_erp_format = "/"
      category_path = category_erp_format.split('/')

      if category_path.count == 2

        parent_category = Category.where('name=?', category_path[0].titleize).last
        rewrite_category = category_path[1]
        right_category = Category.where('name=? and parent_category_id=?', rewrite_category.titleize, parent_category.id).first
      else
        category_one = Category.where('name=?', category_path[0].titleize).last
        category_two = Category.where('name=? and parent_category_id=?', category_path[1].titleize, category_one.id).first
        category_three = Category.where('name=? and parent_category_id=?', category_path[2].titleize, category_two.id).first
        right_category = category_three
      end

      return right_category

      # category_path.reduce(nil) do |parent, category_name|
        # stripped_category_name = category_name.strip
        # if (parent == nil)
        #   return find_by parent_category_id: nil, name: stripped_category_name
        # else
        #   return parent.children_categories.find_by name: stripped_category_name
        # end
      # end
    end
  end


  # def self.get_from_erp_format category_erp_format
  #   if category_erp_format == nil
  #     Category.first
  #   else
  #     category_erp_format = "/"
  #     category_path = category_erp_format.split('/')
  #     category_path.reduce(nil) do |parent, category_name|
  #       stripped_category_name = category_name.strip
  #       if (parent == nil)
  #         find_by parent_category_id: nil, name: stripped_category_name
  #       else
  #         parent.children_categories.find_by name: stripped_category_name
  #       end
  #     end
  #   end
  # end

  

  def self.create_or_update_from_vtex vtex_category, parent_category = nil
    category = find_by vtex_id: vtex_category[:id]
    if (category == nil)
      category = new vtex_id: vtex_category[:id]
    end
    category.name = vtex_category[:name]
    category.parent_category = parent_category

    category.save

    vtex_category[:children].each do |child_category|
      create_or_update_from_vtex child_category, category
    end

    category
  end
end
