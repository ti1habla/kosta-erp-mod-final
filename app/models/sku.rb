class Sku < ApplicationRecord
  belongs_to :product, inverse_of: :skus
  has_many :images, inverse_of: :sku

  include PersistedComparable

  MAP_KEYS_ERP = {
    erp_id: :sku,
    stock: :stock,
    name: :nombre,
    price: :precio,
    weight: :peso,
    height: :alto,
    width: :ancho,
    length: :largo,
    size: :talla,
    color: :color,
  }.freeze

  def initialize *args
    @new_images = nil

    super(*args)
  end

  def new_images
    @new_images
  end

  def new_images= new_images
    @new_images = new_images
  end

  def vtex_web_service_serialize
    {
      "tem:stockKeepingUnitVO" => {
        "vtex:CubicWeight" => weight,
        "vtex:Height" => height,
        "vtex:Id" => vtex_id,
        "vtex:IsActive" => true,
        "vtex:IsKit" => false,
        "vtex:Length" => length,
        "vtex:ModalId" => VTEX_DEFAULT_MODAL_ID,
        "vtex:Name" => name,
        "vtex:ProductId" => product.vtex_id,
        "vtex:RefId" => erp_id,
        "vtex:WeightKg" => weight,
        "vtex:Width" => width
      }
    }
  end

  def vtex_web_service_serialize_size
    Vtex::WebService::Serialization.serialize_especification vtex_id, 'Talla', size, true
  end

  def vtex_web_service_serialize_color
    Vtex::WebService::Serialization.serialize_especification vtex_id, 'Color', color, true
  end

  def data_modified?
    modified?(:height) ||
    modified?(:vtex_id) ||
    modified?(:length) ||
    modified?(:name) ||
    modified?(:product) ||
    modified?(:erp_id) ||
    modified?(:weight) ||
    modified?(:width)
  end

  def size_modified?
    modified? :size
  end

  def color_modified?
    modified? :color
  end

  def stock_modified?
    modified? :stock
  end

  def price_modified?
    modified? :price
  end

  def modified_images
    new_images = self.new_images

    if new_images != nil && new_images.length > 0
      Image.new_and_persisted_modified new_images, :digest, [], { sku_id: self.id }
    else
      []
    end
  end

  def modify_and_set_modified_flags! new_object, ignore = []
    super_return = super(new_object, ignore)

    self.new_images = new_object.new_images

    if !self.new_images.nil?
      self.new_images.each do |image|
        image.sku = self
      end
    end

    super_return
  end

  def self.new_from_erp_format erp_sku
    # sku_hash = {}
    # MAP_KEYS_ERP.each do |key_model, key_erp|
    #   sku_hash[key_model] = erp_sku[key_erp]
    # end

    height_mod = erp_sku["alto"] == nil ? 5 : erp_sku["alto"]
    width_mod = erp_sku["ancho"] == nil ? 5 : erp_sku["ancho"]
    color_mod = erp_sku["color"] == "" ? "4" : erp_sku["color"]
    size_mod = erp_sku["talla"] == nil ? "5" : erp_sku["talla"]
    price_mod = erp_sku["precio"] == "" ? "1" : erp_sku["precio"]
    weight_mod = erp_sku["peso"] == nil ? 5 : erp_sku["peso"]
    name_mod = erp_sku["nombre"] == "" ? "Sku Mod: "+rand(1000..30000).to_s : erp_sku["nombre"]
    length_mod = erp_sku["largo"] == nil ? 5 : erp_sku["largo"]
    stock_mod = erp_sku["stock"] == nil ? 1 : erp_sku["stock"]

    sku_hash = {
      height: height_mod,
      width: width_mod,
      color: color_mod,
      size: size_mod,
      price: price_mod,
      weight: weight_mod,
      name: name_mod,
      length: length_mod,
      stock: stock_mod
    }

    images_erp = erp_sku["imagenes"]

    # sku = new sku_hash
    if !Sku.exists?(erp_id: erp_sku["sku"])
      sku = new sku_hash
    else
      sku = Sku.find_by_erp_id(erp_sku["sku"])
    end
    # =============
    
    if !images_erp.nil?
      new_images = images_erp.map do |image_erp|
        image = Image.new_from_erp_format image_erp
        image.sku = sku
        image    
      end
    end

    sku.new_images = new_images
    sku
  end

  def self.new_and_persisted_modified skus
    super(skus, :erp_id, [:vtex_id])
  end
end
