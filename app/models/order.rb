class Order < ApplicationRecord
  has_many :order_items, inverse_of: :order
  has_one :client_info, inverse_of: :order

  def to_cop value
    value / (10 ** 2)
  end

  def cop_value
    self.to_cop self.value
  end

  def cop_items_value
    self.to_cop self.items_value
  end

  def cop_discounts_value
    if self.discounts_value.nil?
      dis_val = 0
    else
      dis_val = self.discounts_value
    end
    self.to_cop dis_val
  end

  def cop_shipping_value(val)
    self.to_cop val
  end

  def cop_tax_value
    self.to_cop self.tax_value
  end

  def erp_serialize
    {
      orden_id: self.order_id,
      fecha_creacion: self.creation_date,
      cliente: self.client_info.erp_serialize,
      items: self.order_items.map { |item| item.erp_serialize },
      valor_total: self.cop_value,
      metodo_de_pago: self.payment_method,
    }
  end

  def save *args
    super_return = super(*args)

    self.client_info.save
    self.order_items.each { |item| item.save }

    super_return
  end

  def self.new_from_vtex_format order_detail
    address_data = order_detail["shippingData"]["address"]
    
    street = address_data["street"]
    number = address_data["number"]
    complement = address_data["complement"]

    address = ''
    address << "#{street} " if street
    address << "#{number} " if number
    address << "#{complement}" if complement

    address.strip!

    client_data = order_detail["clientProfileData"]

    if address_data["neighborhood"].nil?
      neighborhood_l = "na"
    else
      neighborhood_l = address_data["neighborhood"]
    end

    client_info = ClientInfo.new({
      first_name: client_data["firstName"],
      last_name: client_data["lastName"],
      document: client_data["document"],
      email: client_data["email"],
      phone: client_data["phone"],
      address: address,
      neighborhood: neighborhood_l,
      city: address_data["city"],
      state: address_data["state"],
      country: address_data["country"],
    })

    payment_method = order_detail["paymentData"]["transactions"].reduce(
      ''
    ) do |transactions_reduction, transaction|
      payments_reduction = transaction["payments"].reduce '' do |pr_partial, payment|
        pr_partial << "#{payment["paymentSystemName"]}, "
      end
      transactions_reduction << payments_reduction
    end

    payment_method.chomp! ', '

    decimal_digits = order_detail["storePreferencesData"]["currencyFormatInfo"]["CurrencyDecimalDigits"]

    order_items = order_detail["items"].map do |item|
      dimensions = item["additionalInfo"]["dimension"]
      OrderItem.new({
        vtex_id: item["id"],
        erp_id: item["refId"],
        quantity: item["quantity"],
        selling_price: item["sellingPrice"],
        price: item["price"],
        cubicweight: dimensions["cubicweight"],
        height: dimensions["height"],
        length: dimensions["length"],
        weight: dimensions["weight"],
        width: dimensions["width"],
      })
    end

    totals_hash = {}

    order_detail["totals"].each do |total|
      totals_hash[total["id"].to_sym] = total["value"]
    end

    self.new({
      order_id: order_detail["orderId"],
      status: order_detail["status"],
      value: order_detail["value"],
      decimal_digits: decimal_digits,
      creation_date: order_detail["creationDate"],
      payment_method: payment_method,
      items_value: totals_hash["Items"],
      discounts_value: totals_hash["Discounts"],
      shipping_value: totals_hash["Shipping"],
      tax_value: totals_hash["Tax"],
      client_info: client_info,
      order_items: order_items,
    })
  end
end
