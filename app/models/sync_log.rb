class SyncLog < ApplicationRecord
  has_and_belongs_to_many :products

  def self.most_recent
    SyncLog.order(:timestamp).last
  end
end
