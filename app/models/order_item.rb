class OrderItem < ApplicationRecord
  belongs_to :order, inverse_of: :order_items

  def cop_price
    self.order.to_cop self.price
  end

  def cop_selling_price
    self.order.to_cop self.selling_price
  end

  def erp_serialize
    {
      cantidad: self.quantity,
      sku: self.erp_id,
      precio_de_venta: self.cop_selling_price,
      precio: self.cop_price,
    }
  end
end
