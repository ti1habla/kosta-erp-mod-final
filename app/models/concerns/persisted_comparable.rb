module PersistedComparable
  def self.included(base)
    base.extend(ClassMethods)
    base.send(:include, InstanceMethods)
  end

  module ClassMethods
    def new_and_persisted_modified objects, select_key = self.primary_key, ignore = [], where_hash = {}

      sorted_objects = objects.sort do |object_a, object_b|
        object_a.send(select_key) <=> object_b.send(select_key)
      end

      select_key_values = sorted_objects.map { |object| object.send select_key }

      where_hash[select_key] = select_key_values

      persisted_objects = self.where(where_hash).order(select_key)
      persisted_size = persisted_objects.size

      index = 0

      new_and_persisted_modified = sorted_objects.map do |object|
        persisted_object = nil

        if index < persisted_size
          persisted_object = persisted_objects[index]
        end

        if persisted_object && object.send(select_key) == persisted_object.send(select_key)
          index += 1
          persisted_object.modify_and_set_modified_flags! object, ignore
          persisted_object
        else
          object.set_new
          object
        end
      end

      new_and_persisted_modified
    end
  end

  module InstanceMethods

    def modified? key
      @modified_flags = @modified_flags || {}
      !!@modified_flags[key]
    end

    def set_modified key, value
      @modified_flags = @modified_flags || {}
      @modified_flags[key] = value
    end

    def set_new
      except = ['created_at', 'updated_at']
      self.attributes.except(*except).keys.each do |attribute_key|
        self.set_modified attribute_key.to_sym, true
      end
    end

    def modify_and_set_modified_flags! new_object, ignore = []
      associations = self.class.reflect_on_all_associations(:belongs_to)
      map_associations = {}

      associations.each do |association|
        map_associations[association.foreign_key] = association.name
      end

      except = ignore.map { |ignore_key| ignore_key.to_s }

      except.push(self.class.primary_key, 'created_at', 'updated_at')
      attributes = self.attributes.except(*except).keys

      attributes.each do |attribute_key|
        attribute_key_sym = map_associations[attribute_key]
        if !attribute_key_sym
          attribute_key_sym = attribute_key.to_sym
        end

        object_value = self.send attribute_key_sym
        new_object_value = new_object.send attribute_key_sym

        if object_value != new_object_value
          setter_sym = (attribute_key_sym.to_s << '=').to_sym
          self.send(setter_sym, new_object_value)
          self.set_modified attribute_key_sym, true
        else
          self.set_modified attribute_key_sym, false
        end
      end
    end
  end
end
