class Brand < ApplicationRecord
  def self.create_or_update_from_vtex vtex_brand
    brand = find_by vtex_id: vtex_brand[:id]
    if (brand == nil)
      brand = new vtex_id: vtex_brand[:id]
    end

    brand.name = vtex_brand[:name]

    brand.save
  end
end
