namespace :ka_integration do
  desc "This task synchronizes existing categories in Kosta Azul VTEX E-commerce to this app"
  task sync_categories_from_vtex: :environment do
    vtex_categories = Vtex::Api::Client.get_category_tree.body
    vtex_categories.each do |vtex_category|
      Category.create_or_update_from_vtex vtex_category
    end 
  end

  desc "This task synchronizes existing brands in Kosta Azul VTEX E-commerce to this app"
  task sync_brands_from_vtex: :environment do
    vtex_brands = Vtex::Api::Client.get_brands.body
    vtex_brands.each do |vtex_brand|
      Brand.create_or_update_from_vtex vtex_brand
    end
  end

  desc "This task synchronizes new and modified products and skus since last synchroniztion from Kosta Azul ERP to VTEX and this app"
  task sync_products_and_skus_from_erp_to_vtex: :environment do
    last_sync_log = SyncLog.most_recent
    last_sync_timestamp = last_sync_log ? last_sync_log.timestamp : 0

    now_sync_log = SyncLog.new timestamp: Time.now.to_i
    erp_products_response = Erp::Api::Client.get_products_since(last_sync_timestamp)

    products_erp = erp_products_response.map do |product_erp|
      Product.new_from_erp_format product_erp
    end
    
    new_and_modified_products = Product.new_and_persisted_modified products_erp

    # @permitted_products = [81515,83802,83413,81511,79470,83796,84037,83425,79405,83833,
    #   83954,83570,79409,83961,83429,80137,83795,76189,83925,84198,83494,83928,81517,84108]

    new_and_modified_products.map do |product|
      
      cond = true

      if cond == true and product.category_id != nil and product.page_title != ""

      #if @permitted_products.include?(product.erp_id.to_i)
      # if 79409 == product.erp_id.to_i || 79409 == product.erp_id.to_i || 79409 == product.erp_id.to_i
          
        if product.data_modified?
          product_vtex_id = Vtex::WebService::Client.insert_or_update_product_and_get_id(
            product.vtex_web_service_serialize
          )
          product.vtex_id = product_vtex_id.to_i

          if Product.exists?(vtex_id: product.vtex_id)
            if product.vtex_id != 0 && product.vtex_id != nil
              puts "--------------------------------- Para actualizar: "+product.erp_id.to_s
              # pp product
              puts "---------------------------------"
              product.save
              puts "Guardado: "+product.erp_id.to_s
            else
              puts "No Guardado :("
            end
          else
            product.save
            puts "--------------------------------- Para guardar: "+product.erp_id.to_s
          end
          # categoria_manual = Category.get_from_erp_format product.categoria_total

          # p = product
          # p.category_id = categoria_manual
          # p.save

        end
        
        if product.data_modified?
          product_vtex_id = Vtex::WebService::Client.insert_or_update_product_and_get_id(
            product.vtex_web_service_serialize
          )
          product.vtex_id = product_vtex_id.to_i
          product.save
        end

        # Cares
        if product.cares != 0 and product.cares != "0" and product.cares != nil
          Vtex::Api::Client.insert_product_specification product.vtex_id, product.cares
          product.save
        end


        # if product.cares_modified?
        #   product.cares = cares
        #   Vtex::WebService::Client.insert_product_especification product.vtex_web_service_serialize_cares
        #   product.save
        # end

        # product.cares = cares


        erp_skus_response = Erp::Api::Client.get_skus_of(product.erp_id)

        skus_erp = erp_skus_response.map do |sku_erp|
          sku = Sku.new_from_erp_format sku_erp
          sku.product = product
          sku.erp_id = sku_erp["sku"] # added by kapc
          sku.price = sku_erp["precio"]
          sku.size = sku_erp["talla"]
          sku.color = sku_erp["color"]
          sku.stock = sku_erp["stock"] # added by kapc
          sku
        end

        sku_count = 0

        new_and_modified_skus = Sku.new_and_persisted_modified skus_erp

        @collect_skus = []
        
        new_and_modified_skus.each do |sku|
          sku_count = sku_count+1
          
          if sku_count < 50

            size = sku.size
            sku.size = nil

            color = sku.color
            sku.color = nil

            stock = sku.stock
            sku.stock = nil

            price = sku.price
            sku.price = nil

            if sku.data_modified?
              result = Vtex::WebService::Client.insert_or_update_sku_and_get_result sku.vtex_web_service_serialize
              sku.vtex_id = result[:id]
              sku.is_active = result[:is_active]
              sku.save
            end



            # Tallas
            Vtex::Api::Client.insert_size sku.vtex_id, size
            sku.save


            #Color
            if color != 0 and color != "0" and color != nil
              Vtex::Api::Client.insert_color sku.vtex_id, color
              sku.save
            end

            
            sku.stock = stock
            Vtex::Api::Client.update_sku_stock sku.vtex_id, stock
            sku.save
           
            sku.stock = stock


            if sku.price_modified?
              sku.price = price
              Vtex::Api::Client.update_sku_price sku.vtex_id, sku.price
              sku.save
            end

            sku.price = price
                
            # sku.modified_images.each do |image|
            #   if image.data_modified?
            #     Vtex::WebService::Client.insert_or_update_image image.vtex_web_service_serialize
            #     image.save
            #   end
            # end

            sku.new_images = nil
            
            if (sku.modified_images != [])
              pp "entré id"
              Vtex::WebService::Client.activate_sku sku.vtex_id
              sku.is_active = true
              sku.save
            end

            if sku.name[0...3] != "Sku"
              begin
                Vtex::WebService::Client.activate_sku sku.vtex_id
                sku.is_active = true
                sku.save
              rescue
                puts "sin imagen"
              end
            end

            pp "Guardó sku: "+sku.erp_id.to_s
          else
            #Skus mayores a 50, no guardaron
            @collect_skus.push(sku.erp_id)
          end

        end

        if @collect_skus.length > 0
          OrdersMailer.skus_no_saved(@collect_skus).deliver
        end

        #now_sync_log.products << product
        now_sync_log.save
      
      end


      
    end
  end




  desc "This task synchronizes ready for handling and canceled orders from VTEX to Kosta Azul ERP"
  task sync_orders_from_vtex_to_erp: :environment do
    orders_feed = Vtex::Api::Client.get_orders_feed.body
    orders_feed = JSON.parse(orders_feed)
    orders_feed = orders_feed["list"]

    pp "entré a las ordenes"
    
    commit_tokens = []

    orders_feed.each do |order_feed|
      order_id = order_feed["orderId"]
      status = order_feed["status"]
      commit_token = order_feed["commitToken"]
      datetime = DateTime.parse order_feed["creationDate"]
      
      case status
      when 'ready-for-handling', 'cancellation-requested'#, 'handling'
        order_detail = JSON.parse(Vtex::Api::Client.get_order_detail(order_id).body)
        vtex_status = order_detail["status"]
        case vtex_status
        when 'ready-for-handling', 'cancellation-requested'#, 'handling'
          local_order = Order.find_by order_id: order_id
          is_success = true

          #local_order
          if !local_order
            is_success = false
            local_order = Order.new_from_vtex_format order_detail
            local_order.status = 'ready-for-handling'

            order_erp_hash = local_order.erp_serialize

            precio_total = 0
            order_erp_hash[:items].each do |i|
              precio_total = precio_total + i[:precio]
            end

            flete = order_erp_hash[:valor_total] - precio_total
            order_erp_hash[:items].push({:cantidad=>1, :sku=>"FL000011U", :precio_de_venta=>flete, :precio=>flete})
            #order_erp_hash[:porcentajeiva].push((precio_total*19)/100)

            # abort order_erp_hash.inspect

            response_body = Erp::Api::Client.create_order(order_erp_hash).body
            response_body = JSON.parse(response_body)

            if response_body["resultado"] == "ok"
              local_order.save
              is_success = true
            end
          end

          if is_success
            pp "Is_success"
            # if !local_order.datetime # así era originalmente
            if !local_order.datetime #|| !local_order.datetime
              local_order.datetime = datetime
              local_order.save
              pp "Orden local creada"

              case vtex_status
              when 'ready-for-handling', 'handling'
                  response = Vtex::Api::Client.order_start_handling order_id
                  local_order.status = 'handling'
                  local_order.save
                  commit_tokens << commit_token

                  # TODO: Generate Servientrega shipping guide
                  #  * Order items are in local_order.order_items. Each order_item has
                  #    -> quantity
                  #    -> price (COP cents)
                  #    -> cop_price (COP) (Use this)
                  #    -> cubicweight
                  #    -> height (cm)
                  #    -> length (cm)
                  #    -> weight (g)
                  #    -> width (cm)
                  #  * Client info is in local_order.client_info. Client info has
                  #    -> first_name
                  #    -> last_name
                  #    -> document
                  #    -> email
                  #    -> address
                  #    -> neighborhood
                  #    -> city
                  #    -> state
                  #    -> country

                  total_volume = 0
                  total_weight = 0
                  items_quantity = 0
                  total_price = 0
                  total_height = 0
                  total_width = 0
                  total_length = 0

                  local_order.order_items.each do |product|
                    items_quantity += product.quantity
                    total_price += product.cop_price
                    volume = product.height * product.length * product.width
                    total_volume += volume
                    total_weight += product.weight
                    total_height += product.height
                    total_width += product.width
                    total_length += product.length
                  end


                  client_info = local_order.client_info

                  guide_data = {
                    total_volume: total_volume,
                    total_weight: total_weight,
                    items_quantity: items_quantity,
                    total_price: total_price,
                    first_name: client_info.first_name,
                    last_name: client_info.last_name,
                    document: client_info.document,
                    email: client_info.email,
                    address: client_info.address,
                    neighborhood: client_info.neighborhood,
                    city: client_info.city,
                    state: client_info.state,
                    country: client_info.country,
                    cop_shipping_value: (local_order.cop_shipping_value(Order.find_by_order_id(order_id).value) - total_price),
                    total_height: total_height,
                    total_width: total_width,
                    total_length: total_length
                  }

                  guide_number = Api::V1::TrackingController.generateGuide(guide_data)

                  OrdersMailer.send_guide_servientrega(guide_data[:email], guide_number[:guide_number]).deliver_now

                  pp "Guia generada"
              when 'cancellation-requested'
                if !local_order.status || local_order.status != vtex_status
                  local_order.status = vtex_status
                  erp_status = 'cancelacion-solicitada'

                  response_body = Erp::Api::Client.update_order_status(order_id, erp_status).body
                  case response_body[:resultado]
                  when 'ok'
                    local_order.save
                    commit_tokens << commit_token
                  end
                else
                  local_order.save
                  commit_tokens << commit_token
                end
              end
            else
              commit_tokens << commit_token
            end
          end
        else
          commit_tokens << commit_token
        end
      else
        commit_tokens << commit_token
      end

    end
    # Vtex::Api::Client.ack_orders_feed commit_tokens
  end
end
